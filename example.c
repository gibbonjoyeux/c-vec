/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include <stdio.h>

#include "vec.h"

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

/// DEFINE YOUR OWN VECTOR STRUCTURE
/// HERE I DECIDED TO USE double TYPE
typedef struct s_vec	t_vec;
struct			s_vec {
	double		x;
	double		y;
};

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			main(void) {
	/// DECLARE MY VECTORS
	t_vec	a;
	t_vec	b;
	t_vec	c;
	double	i, j;

	/// CREATE / SET A VECTOR
	VEC_SET(a, 3, 0);
	/// GET ITS ANGLE
	i = VEC_HEADING(a);
	/// EASILY PRINT IT
	printf("%lf %lf: %lf\n", VEC_LIST(a), i);

	/// ROTATE THE VECTOR
	VEC_ROTATE(a, 3.141592653);
	/// GET ITS ANGLE AGAIN
	i = VEC_HEADING(a);
	printf("%lf %lf: %lf\n", VEC_LIST(a), i);

	/// NORMALIZE THE VECTOR
	VEC_NORMALIZE(a);
	/// GET ITS ANGLE AGAIN
	i = VEC_HEADING(a);
	printf("%lf %lf: %lf\n", VEC_LIST(a), i);

	/// CONSTRAIN THE VECTOR TO A MAGNITUDE RANGE
	VEC_CONSTRAIN(a, 1, 4);
	printf("%lf %lf\n", VEC_LIST(a));
	return 0;
}
