# c-vec

This is a small C / CPP 2D **vector** library.

It consists of a single `.h` file defining a list of **macro functions**.

This way of working has multiple advantages. It is **fast** as it does not call real functions but does everything inline, it allows you to easily use vectors **without any memory allocation** and also allows you to use your **own data structures** (while it contains `x` and `y` fields), and thus, the **number type you want** (double, float, int, etc.).

## ABOUT

You can open the `example.c` to see a use example of the lib.

Some functions have a `..._TO` alternative consisting of the exact same function but putting the result into another vector passed as last parameter.

Bezier curve functions work the same way `..._TO` functions do. They require the vector to put the result in as last parameter.

Some functions acts exactly the same way but are named differently. This is thought to make your code clearer depending on context (ex: `VEC_ADD` & `VEC_MULT` could be used in math context while `VEC_TRANSLATEV` & `VEC_SCALE` would be used in space transformation context).

## FUNCTIONS

- RETURNING FUNCTIONS:
	- a.x, a.y	`VEC_LIST(a)`
	- double	`VEC_MAG(a)`						-> Returns vec magnitude
	- double	`VEC_MAG_SQ(a)`						-> Returns vec magnitude squared
	- double	`VEC_DIST(a, b)`					-> Returns distance between 2 vec
	- double	`VEC_HEADING(a)`					-> Returns vec angle
	- double	`VEC_ANGLE_BETWEEN(a, b)`			-> Returns angle between 2 vec
- ACTION FUNCTIONS:
	- BASICS:
		- `VEC_SET(a, x, y)`						-> Init vec
		- `VEC_SET_ANGLE(a, angle)`					-> Init vec from angle
		- `VEC_COPY(a, b)`							-> Copy vec
	- MATHS:
		- `VEC_ADD(a, b)`							-> Add 2 vec
		- `VEC_SUB(a, b)`							-> Sub 2 vec
		- `VEC_MULT(a, scalar)`						-> Mult vec by `scalar`
		- `VEC_DIV(a, scalar)`						-> Div vec by `scalar`
		- `VEC_LERP(a, b, t)`						-> Linear interpolation between 2 vec
		- `VEC_ADD_TO(a, b, c)`
		- `VEC_SUB_TO(a, b, c)`
		- `VEC_MULT_TO(a, scalar, c)`
		- `VEC_DIV_TO(a, scalar, c)`
		- `VEC_LERP_TO(a, b, t, c)`
	- BEZIER CURVE:
		- `VEC_BEZIER_LINEAR(p0, p1, t, a)`			-> Linear bezier curve interpolation
		- `VEC_BEZIER_QUAD(p0, p1, p2, t, a)`			-> Quadratic bezier curve interpolation
		- `VEC_BEZIER_CUBIC(p0, p1, p2, p3, t, a)`		-> Cubic bezier curve interpolation
	- TRANSFORM:
		- `VEC_SCALE(a, scale)`						-> Scale vec by `scale` (MULT)
		- `VEC_ROTATE(a, angle)`					-> Rotate vec by `angle`
		- `VEC_TRANSLATE(a, b)`						-> Translate vec by another vec (ADD)
		- `VEC_TRANSLATE2(a, x, y)`					-> Translate vec by `x` & `y`
		- `VEC_TRANSFORM(a, b, angle, scale)`		-> Transform vec in space
		- `VEC_TRANSFORM2(a, x, y, angle, scale)`	-> Transform vec in space
		- `VEC_SCALE_TO(a, scale, b)`
		- `VEC_ROTATE_TO(a, angle, b)`
		- `VEC_TRANSLATE_TO(a, b, c)`
		- `VEC_TRANSLATE2_TO(a, x, y, b)`
		- `VEC_TRANSFORM_TO(a, b, angle, scale)`
		- `VEC_TRANSFORM2_TO(a, x, y, angle, scale)`
	- LENGTH:
		- `VEC_NORMALIZE(a)`						-> Normalize vec
		- `VEC_SET_MAG(a, magnitude)`				-> Set vec magnitude
		- `VEC_NORMALIZE_TO(a, b)`
		- `VEC_SET_MAG_TO(a, magnitude, b)`
	- CONSTRAIN:
		- `VEC_MIN(a, min)`							-> Limit vec min value
		- `VEC_MAX(a, max)`							-> Limit vec max value
		- `VEC_CONSTRAIN(a, min, max)`				-> Constrain vec in min / max range
		- `VEC_MIN_TO(a, min, b)`
		- `VEC_MAX_TO(a, max, b)`
		- `VEC_CONSTRAIN_TO(a, min, max, b)`

## NOTE

The main drawback with the macro way of working is that it can be more difficult to fixe synthax errors. Note also that, as macros are technically text substitions, it is better (for speed performance) to pass these functions variables instead of expressions.

Example:
```
	/// METHOD 1: CORRECT BUT NOT MOST EFFICIENT
	/// -> PASS EXPRESSION
	VEC_LERP(vec_from, vec_to, i / length);

	/// METHOD 2: CORRECT AND MORE EFFICIENT
	/// -> PASS ALREADY COMPUTED VALUE
	double	t = i / length;
	VEC_LERP(vec_from, vec_to, t);
```

