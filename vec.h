/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////
//
// - a.x, a.y	VEC_LIST(a);
//
// - double		VEC_MAG(a);					Returns vec magnitude
// - double		VEC_MAG_SQ(a);				Returns vec magnitude squared
// - double		VEC_DIST(a, b);				Returns distance between 2 vec
// - double		VEC_HEADING(a);				Returns vec angle
// - double		VEC_ANGLE_BETWEEN(a, b);	Returns angle between 2 vec
//
// BASICS:
// - VEC_SET(a, x, y);						Init vec
// - VEC_SET_ANGLE(a, angle);				Init vec from angle
// - VEC_COPY(a, b);						Copy vec
// MATHS:
// - VEC_ADD(a, b);							Add 2 vec
// - VEC_SUB(a, b);							Sub 2 vec
// - VEC_MULT(a, scalar);					Mult vec by scalar
// - VEC_DIV(a, scalar);					Div vec by scalar
// - VEC_LERP(a, b, t);						Linear interpolation from 2 vec
// - VEC_ADD_TO(a, b, c);					//
// - VEC_SUB_TO(a, b, c);					//
// - VEC_MULT_TO(a, scalar, c);				//
// - VEC_DIV_TO(a, scalar, c);				//
// - VEC_LERP_TO(a, b, t, c);				//
// BEZIER CURVE:
// - VEC_BEZIER_LINEAR(a, b, t, c)			Linear bezier curve interpolation
// - VEC_BEZIER_QUAD(a, b, c, t, d)			Quadratic bezier curve interpolation
// - VEC_BEZIER_CUBIC(a, b, c, d, t, e)		Cubic bezier curve interpolation
// TRANSFORM:
// - VEC_SCALE(a, scale)					Scale vec by `scale` (MULT)
// - VEC_ROTATE(a, angle);					Rotate vec by `angle`
// - VEC_TRANSLATE(a, b)					Translate vec by another vec (ADD)
// - VEC_TRANSLATE2(a, x, y)				Translate vec by `x` & `y`
// - VEC_TRANSFORM(a, b, angle, scale)		Transform vec in space
// - VEC_TRANSFORM2(a, _x, _y, angle, scale)	Transform vec in space
// - VEC_SCALE_TO(a, scale, b)				//
// - VEC_ROTATE_TO(a, angle, b);			//
// - VEC_TRANSLATE_TO(a, b, c)				//
// - VEC_TRANSLATE2_TO(a, x, y, b)			//
// - VEC_TRANSFORM_TO(a, b, angle, scale)	//
// - VEC_TRANSFORM2_TO(a, _x, _y, angle, scale)	//
// LENGTH:
// - VEC_NORMALIZE(a);						Normalize vec
// - VEC_SET_MAG(a, magnitude);				Set vec magnitude
// - VEC_NORMALIZE_TO(a, b);				//
// - VEC_SET_MAG_TO(a, magnitude, b);		//
// CONSTRAIN:
// - VEC_MIN(a, min);						Limit vec min value
// - VEC_MAX(a, max);						Limit vec max value
// - VEC_CONSTRAIN(a, min, max);			Constrain vec in min / max range
// - VEC_MIN_TO(a, min, b);					//
// - VEC_MAX_TO(a, max, b);					//
// - VEC_CONSTRAIN_TO(a, min, max, b);		//
//

#ifndef VEC_H
# define VEC_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <math.h>

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

#define VEC_A							g_vec_a
#define VEC_B							g_vec_b

#define POW2(x)							((x) * (x))
#define POW3(x)							((x) * (x) * (x))

//////////////////////////////////////////////////
/// RETURNING MACROS
//////////////////////////////////////////////////

#define VEC_MAG(a)						(sqrt((a).x * (a).x + (a).y * (a).y))
#define VEC_MAG_SQ(a)					(VEC_A = VEC_MAG(a), \
										VEC_A * VEC_A)
#define VEC_DIST(a, b)					(VEC_A = (a).x - (b).x, \
										VEC_B = (a).y - (b).y, \
										sqrt(VEC_A * VEC_A \
										+ VEC_B * VEC_B))
#define VEC_HEADING(a)					(atan2((a).y, (a).x))
#define VEC_ANGLE_BETWEEN(a, b)			(atan2((a).y - (b).y, \
										(a).x - (b).x))
#define VEC_LIST(a)						(a).x, (a).y

//////////////////////////////////////////////////
/// ACTION MACROS
//////////////////////////////////////////////////

//////////////////////////////
/// BASICS
//////////////////////////////

#define VEC_SET(a, _x, _y)				(a).x = (_x); \
										(a).y = (_y);
#define VEC_SET_ANGLE(a, angle)			(a).x = cos(angle); \
										(a).y = sin(angle);
#define VEC_COPY(a, b)					(a).x = (b).x; \
										(a).y = (b).y;

//////////////////////////////
/// MATH
//////////////////////////////

#define VEC_ADD(a, b)					(a).x += (b).x; \
										(a).y += (b).y;
#define VEC_SUB(a, b)					(a).x -= (b).x; \
										(a).y -= (b).y;
#define VEC_MULT(a, scalar)				(a).x *= scalar; \
										(a).y *= scalar;
#define VEC_DIV(a, scalar)				(a).x /= scalar; \
										(a).y /= scalar;
#define VEC_LERP(a, b, t)				(a).x = (a).x + t * ((b).x - (a).x); \
										(a).y = (a).y + t * ((b).y - (a).y);

#define VEC_ADD_TO(a, b, c)				(c).x = (a).x + (b).x; \
										(c).y = (a).x + (b).y;
#define VEC_SUB_TO(a, b, c)				(c).x = (a).x - (b).x; \
										(c).y = (a).x - (b).y;
#define VEC_MULT_TO(a, scalar, b)		(b).x = (a).x * scalar; \
										(b).y = (a).y * scalar;
#define VEC_DIV_TO(a, scalar, b)		(b).x = (a).x / scalar; \
										(b).y = (a).y / scalar;
#define VEC_LERP_TO(a, b, t, c)			(c).x = (a).x + t * ((b).x - (a).x); \
										(c).y = (a).y + t * ((b).y - (a).y);

//////////////////////////////
/// BEZIER
//////////////////////////////

#define VEC_BEZIER_LINEAR(p0, p1, t, a)	VEC_LERP_TO(p0, p1, t, a);
#define VEC_BEZIER_QUAD(p0, p1, p2, t, a)	\
										VEC_A = (1 - (t)); \
										(a).x = POW2(VEC_A) * (p0).x \
										+ 2 * VEC_A * (t) * (p1).x \
										+ POW2(t) * (p2).x; \
										(a).y = POW2(VEC_A) * (p0).y \
										+ 2 * VEC_A * (t) * (p1).y \
										+ POW2(t) * (p2).y;
#define VEC_BEZIER_CUBIC(p0, p1, p2, p3, t, a) \
										VEC_A = (1 - (t)); \
										(a).x = POW3(VEC_A) * (p0).x \
										+ 3 * POW2(VEC_A) * (t) * (p1).x \
										+ 3 * VEC_A * POW2(t) * (p2).x \
										+ POW3(t) * (p3).x; \
										(a).y = POW3(VEC_A) * (p0).y \
										+ 3 * POW2(VEC_A) * (t) * (p1).y \
										+ 3 * VEC_A * POW2(t) * (p2).y \
										+ POW3(t) * (p3).y;

//////////////////////////////
/// TRANSFORMATION
//////////////////////////////

#define VEC_SCALE(a, scale)				VEC_MULT(a, scale);
#define VEC_TRANSLATE(a, b)				VEC_ADD(a, b);
#define VEC_TRANSLATE2(a, _x, _y)		(a).x += _x; \
										(a).y += _y;
#define VEC_ROTATE(a, angle)			VEC_A = (a).x; \
										VEC_B = (a).y; \
										(a).x = VEC_A * cos(angle) \
										- VEC_B * sin(angle); \
										(a).y = VEC_A * sin(angle) \
										+ VEC_B * cos(angle);
#define VEC_TRANSFORM(a, b, angle, scale) \
										VEC_SCALE(a, scale); \
										VEC_ROTATE(a, angle); \
										VEC_TRANSLATE(a, b);
#define VEC_TRANSFORM2(a, _x, _y, angle, scale) \
										VEC_SCALE(a, scale); \
										VEC_ROTATE(a, angle); \
										VEC_TRANSLATE2(a, _x, _y);
#define VEC_SCALE_TO(a, scale, b)		VEC_MULT_TO(a, scale, b);
#define VEC_TRANSLATE_TO(a, b, c)		(c).x = (a).x + (b).x; \
										(c).y = (a).y + (b).y;
#define VEC_TRANSLATE2_TO(a, _x, _y, b)	(b).x = (a).x + _x; \
										(b).y = (a).y + _y;
#define VEC_ROTATE_TO(a, angle, b)		(b).x = (a).x * cos(angle) \
										- (a).y * sin(angle); \
										(b).y = (a).x * sin(angle) \
										+ (a).y * cos(angle);
#define VEC_TRANSFORM_TO(a, b, angle, scale, c) \
										VEC_COPY(c, a); \
										VEC_SCALE(c, scale); \
										VEC_ROTATE(c, angle); \
										VEC_TRANSLATE(c, b);
#define VEC_TRANSFORM2_TO(a, _x, _y, angle, scale, b) \
										VEC_COPY(b, a); \
										VEC_SCALE(b, scale); \
										VEC_ROTATE(b, angle); \
										VEC_TRANSLATE2(b, _x, _y);

//////////////////////////////
/// LENGTH
//////////////////////////////

#define VEC_NORMALIZE(a)				VEC_A = VEC_MAG(a); \
										if (VEC_A == 0) { \
											(a).x = 0; \
											(a).y = 0; \
										} else { \
											(a).x /= VEC_A; \
											(a).y /= VEC_A; \
										}
#define VEC_SET_MAG(a, length)			VEC_NORMALIZE(a); \
										VEC_MULT(a, length);
#define VEC_NORMALIZE_TO(a, b)			VEC_A = VEC_MAG(a); \
										if (VEC_A == 0) { \
											(b).x = 0; \
											(b).y = 0; \
										} else { \
											(b).x = (a).x / VEC_A; \
											(b).y = (a).y / VEC_A; \
										}
#define VEC_SET_MAG_TO(a, length, b)	VEC_COPY(b, a); \
										VEC_SET_MAG(b, length);

//////////////////////////////
/// CONSTRAIN
//////////////////////////////

#define VEC_MIN(a, min)					if (min > VEC_MAG(a)) { \
											VEC_SET_MAG(a, min); \
										}
#define VEC_MAX(a, max)					if (max < VEC_MAG(a)) { \
											VEC_SET_MAG(a, max); \
										}
#define VEC_CONSTRAIN(a, min, max)		VEC_A = VEC_MAG(a); \
										if (VEC_A > max) { \
											VEC_SET_MAG(a, max); \
										} \
										if (VEC_A < min) { \
											VEC_SET_MAG(a, min); \
										}
#define VEC_MIN_TO(a, min, b)			VEC_COPY(b, a); \
										VEC_MIN(b, min);
#define VEC_MAX_TO(a, max, b)			VEC_COPY(b, a); \
										VEC_MAX(b, max);
#define VEC_CONSTRAIN_TO(a, min, max, c)	\
										VEC_COPY(b, a); \
										VEC_CONSTRAIN(b, min, max);

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

double			g_vec_a;
double			g_vec_b;

#endif
